
package com.example;

public class Main {
    public static void main(String[] args) {
        
        String originalWord= "Anar";
        StringBuilder builder = new StringBuilder(originalWord);
        StringBuilder reversedWord =  builder.reverse();
        String finalResult = reversedWord.toString();
        System.out.println("Reversed Version: " + finalResult);
         
    }
}
